# user managment ui #

This README would normally document whatever steps are necessary to get your application up and running.

### db folder ###

* all sql tables with dummy data 

### Web folder (React js) ###

* run these command to initalise the node modules & package.json
* yarn add react-scripts
* yarn add rsuite
* yarn add sweetalert2
* yarn add @fortawesome/fontawesome-svg-core
* yarn add @fortawesome/free-solid-svg-icons
* yarn add @fortawesome/react-fontawesome
* yarn add @mui/icons-material
* yarn add @mui/material
* yarn add @mui/styled-engine-sc
* yarn add axios
* yarn add jquery
* yarn add react-animated-css
* yarn add react-bootstrap
* yarn add react-dom
* yarn add react-router-dom
* yarn add react-scripts  

### Server folder (Node js) ###

* All Back-end Process
* run these commands to initalize package.json in SERVER folder
* yarn add cors
* yarn add express
* yarn add mysql

 