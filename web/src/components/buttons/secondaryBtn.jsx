import "./button.css";
import React from "react";

export default function SecondaryBtn({ onClick, value }) {
  return (
    <React.Fragment>
      <input
        type="button"
        value={value}
        onClick={onClick}
        className="Cancelbtn marginRIght"
      />
    </React.Fragment>
  );
}
