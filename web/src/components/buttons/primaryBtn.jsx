import "./button.css";
import React from "react";

export default function PrimaryBtn({ onClick, value }) {
  return (
    <React.Fragment>
      <input
        type="button"
        value={value}
        onClick={onClick}
        className="button-61 marginRIght"
      />
    </React.Fragment>
  );
}
