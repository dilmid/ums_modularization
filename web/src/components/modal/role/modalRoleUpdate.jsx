import React, { useState, useEffect } from "react";
import Axios from "axios";
import $ from "jquery";
import Swal from "sweetalert2";
import "../role/modalRoleUpdate.css";
import { MessageOutlined ,AdminPanelSettings} from "@mui/icons-material";
import { Modal, Grid, Whisper, Tooltip, Form, InputGroup, Input } from "rsuite";
import PrimaryBtn from "../../buttons/primaryBtn";
import SecondaryBtn from "../../buttons/secondaryBtn";
import StatusPicker from "../../inputFields/statusPicker";
import HiddenInputField from "../../inputFields/hiddenInputField";
import ModalPermissionAssign from "../permission/modalPermissionAssign";

export default function ModalRoleUpdate({ parentToChild, reloadTable }) {
  const [status, setStatus] = useState("");
  const [statusVal, setStatusVal] = useState();
  const [txtarea, settxtarea] = useState();
  const [RoleData, setRoleData] = useState([]);
  const [modalAssign, setmodalAssign] = useState(false);
  const [roleID, setroleID] = useState({ data: "" });

  const HOST = `${process.env.REACT_APP_HOST}`;
  const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
  const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

  //load role details according to selected role id start
  useEffect(() => {
    Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/getRoleIdData`, {
      RoleidVal: parentToChild,
    }).then((result) => {
      setRoleData(result.data);
      // console.log(RoleData);
    });
  }, [parentToChild]); // eslint-disable-line react-hooks/exhaustive-deps
  //load role details according to selected role id end

  //resetBtn start
  const resetBtn = () => {
    setStatusVal(null);
    settxtarea("");
    $('input[name="inputFields"]').val("");
  };
  //resetBtn end

  //role updateBtn start
  const roleUpdateBtn = () => {
    if (
      $("#roleName").val() === "" ||
      $("#roleInfo").val() === "" ||
      $("#statusInput").val() === "0"
    ) {
      Swal.fire({
        position: "top-end",
        icon: "warning",
        title: "All Field Requried!",
        showConfirmButton: false,
        timer: 1500,
      });
    } else {
      reloadTable();
      Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/updateRole`, {
        Rname: $("#roleName").val(),
        Rdesc: $("#roleInfo").val(),
        status: $("#statusInput").val(),
        Rid: parentToChild,
      }).then(() => {
        console.log("update success");
      });
    }
  };
  //role update btn end

  //permission assigning modal open (check & assign permissions btn)
  const assignModalOpen = () => {
    setmodalAssign(true);
    setroleID({ data: parentToChild });
  };
  //end

  //close permission asssign modal
  const handleClose = () => setmodalAssign(false);
  //end

  return (
    <React.Fragment>
      <Modal size="md" open={modalAssign} onClose={handleClose}>
        <ModalPermissionAssign data={roleID.data} />
      </Modal>

      <Modal.Header>
        <Modal.Title>
          <div className="modalHeading">
            <AdminPanelSettings fontSize="large" /> Role Info
          </div>

          <span className="tagline">Fill out the below form.</span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Grid fluid={true} faded="true" className="roleModal">
          {RoleData.map((val, key) => {
            return (
              <React.Fragment>
                <label>Name :</label>
                <InputGroup className="InputGroup">
                  <InputGroup.Addon>
                    <MessageOutlined />
                  </InputGroup.Addon>
                  <Whisper
                    trigger="focus"
                    speaker={<Tooltip>Required</Tooltip>}
                  >
                    <Input
                      style={{ width: 200 }}
                      placeholder="Role name"
                      type="text"
                      id="roleName"
                      name="inputFields"
                      defaultValue={val.role_name}
                    />
                  </Whisper>
                </InputGroup>
                <label>Description :</label>
                <InputGroup className="InputGroup">
                  <Whisper
                    trigger="focus"
                    speaker={<Tooltip>Required</Tooltip>}
                  >
                    <Input
                      as="textarea"
                      name="inputFields"
                      rows={3}
                      defaultValue={val.role_desc}
                      value={txtarea}
                      id="roleInfo"
                      placeholder="Description"
                      onChange={(event) => {
                        settxtarea(event);
                      }}
                    />
                  </Whisper>
                </InputGroup>

                <Form.Group controlId="radioList">
                  <label>Status :</label>
                  <HiddenInputField
                    name="inputFields"
                    id="statusInput"
                    value={status === "" ? val.role_status : status}
                  />
                  <StatusPicker
                    value={statusVal}
                    id="statusVal"
                    onChange={(event) => {
                      setStatus(event);
                      setStatusVal();
                    }}
                    defaultValue={
                      val.role_status === "Active" ? "Active" : "Inactive"
                    }
                    onClean={() => {
                      setStatus("0");
                    }}
                  />
                </Form.Group>
                <Form.Group controlId="AssignBtn">
                  <PrimaryBtn
                    onClick={assignModalOpen}
                    value="Check & Assign Permissions "
                  />
                </Form.Group>
              </React.Fragment>
            );
          })}
        </Grid>
      </Modal.Body>

      <Modal.Footer className="btnGroupmodal">
        <PrimaryBtn onClick={roleUpdateBtn} value="Update" />
        <SecondaryBtn onClick={resetBtn} value="Reset" />
      </Modal.Footer>
    </React.Fragment>
  );
}
