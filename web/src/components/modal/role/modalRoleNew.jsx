import React, { useState } from "react";
import Axios from "axios";
import $ from "jquery";
import Swal from "sweetalert2";
import "../role/modalRoleUpdate.css";
import {
  Modal,
  Grid,
  Panel,
  Whisper,
  Tooltip,
  Form,
  InputGroup,
  Input,
} from "rsuite";
import { Animated } from "react-animated-css";
import { AdminPanelSettings, MessageOutlined } from "@mui/icons-material";
import PrimaryBtn from "../../buttons/primaryBtn";
import SecondaryBtn from "../../buttons/secondaryBtn";
import StatusPicker from "../../inputFields/statusPicker";
import HiddenInputField from "../../inputFields/hiddenInputField";
import PermissionTypePicker from "../../inputFields/permissionTypePicker";
import PermissionsPicker from "../../inputFields/permissionsPicker";

export default function ModalRoleNew({ reloadTable }) {
  const HOST = `${process.env.REACT_APP_HOST}`;
  const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
  const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

  const [select, setSelect] = useState();
  const [txtarea, settxtarea] = useState();
  const [status, setStatus] = useState();
  const [categoryVal, setcategoryVal] = useState();
  const [permissions, setPermissions] = useState([]);
  const [showPermissionPicker, setshowPermissionPicker] = useState(true);

  //resetBtn start
  const resetBtn = () => {
    setSelect(null);
    settxtarea("");
    setcategoryVal("");
    $('input[name="inputFields"]').val("");
  };
  //resetBTn end

  //insert new role Btn start
  const roleInsert = () => {
    if (
      $("#roleName").val() === "" ||
      $("#roleInfo").val() === "" ||
      permissions.length === 0 ||
      $("#statusVal").val() === "0"
    ) {
      Swal.fire({
        position: "top-end",
        icon: "warning",
        title: "All Field Requried!",
        showConfirmButton: false,
        timer: 1500,
      });
    } else {
      Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/insertRole`, {
        role: $("#roleName").val(),
        info: $("#roleInfo").val(),
        status: $("#statusVal").val(),
        permissions: permissions,
      }).then((response) => {
        if (response.data === "exists") {
          Swal.fire({
            position: "top-end",
            icon: "error",
            title: "Error : Record Exist!!",
            showConfirmButton: false,
            timer: 1500,
          });
        } else {
          console.log("role ssuccess");
        }
      });

      reloadTable();
    }
  };
  //insert new role Btn start

  //show permissions for selected category and show permissons list start
  const displayPermissions = (e) => {
    setcategoryVal(e);
    setshowPermissionPicker(false);
  };
  //end

  //get selected permission values from child component start
  const childToParent = (childdata) => {
    setPermissions(childdata);
  };
  //end
  return (
    <React.Fragment>
      <Modal.Header>
        <Modal.Title>
          <h4>
            <AdminPanelSettings fontSize="large" /> New Role Info
          </h4>

          <span className="tagline">Fill out the below form.</span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Grid fluid={true} faded="true" className="roleModal">
          <InputGroup className="InputGroup">
            <InputGroup.Addon>
              <MessageOutlined />
            </InputGroup.Addon>
            <Whisper trigger="focus" speaker={<Tooltip>Required</Tooltip>}>
              <Input
                style={{ width: 200 }}
                placeholder="Role name"
                type="text"
                id="roleName"
                name="inputFields"
              />
            </Whisper>
          </InputGroup>
          <InputGroup className="InputGroup">
            <Whisper trigger="focus" speaker={<Tooltip>Required</Tooltip>}>
              <Input
                as="textarea"
                name="inputFields"
                rows={3}
                value={txtarea}
                id="roleInfo"
                placeholder="Description"
                onChange={(event) => {
                  settxtarea(event);
                }}
              />
            </Whisper>
          </InputGroup>
          <Form.Group controlId="radioList">
            <HiddenInputField
              name="inputFields"
              id="statusVal"
              value={status}
            />
            <StatusPicker
              value={select}
              onChange={(event) => {
                setStatus(event);
                setSelect();
              }}
              onClean={() => {
                setStatus("0");
              }}
            />
          </Form.Group>

          <Panel header="Assign Permission(s)" bordered className="mt-2">
            <Form.Group className="mt-2">
              <PermissionTypePicker
                onChange={displayPermissions}
                value={categoryVal}
                Placeholder="Select category here..."
                placement="topStart"
              />
            </Form.Group>
            <Form.Group className="mt-2" hidden={showPermissionPicker}>
              <Animated
                animationIn="fadeIn"
                animationOut="fadeOut"
                isVisible={true}
              >
                <PermissionsPicker
                  categoryVal={categoryVal}
                  childToParent={childToParent}
                  placement="topStart"
                />
              </Animated>
            </Form.Group>
          </Panel>
        </Grid>
      </Modal.Body>
      <Modal.Footer className="btnGroupmodal">
        <PrimaryBtn onClick={roleInsert} value="Submit" />
        <SecondaryBtn onClick={resetBtn} value="Reset" />
      </Modal.Footer>
    </React.Fragment>
  );
}
