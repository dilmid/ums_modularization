import React, { useState } from "react";
import Axios from "axios";
import Swal from "sweetalert2";
import "./modalPermissionUpdate.css";
import { Modal, Divider, Form, Row, Grid } from "rsuite";
import { faMagic } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PermissiontTypePicker from "../../inputFields/permissionTypePicker";
import PermissionTagsPicker from "../../inputFields/permissionsTagsPicker";
import PrimaryBtn from "../../buttons/primaryBtn";
import SecondaryBtn from "../../buttons/secondaryBtn";

export default function ModalPermissionUpdate() {
  const [categoryVal, setcategoryVal] = useState("");
  // eslint-disable-next-line
  const [tagStatus, setTagStatus] = useState([]);
  // eslint-disable-next-line
  const [permissions, setPermissions] = useState([]);
  const [deleteMsg, setdeleteMsg] = useState(true);

  const HOST = `${process.env.REACT_APP_HOST}`;
  const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
  const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

  //show permissions for selected category start
  const displayPermissions = (e) => {
    setcategoryVal(e);
    // console.log("test" + categoryVal);
  };
  //end

  //get selected permission values from child component start
  const childToParent = (childdata) => {
    setPermissions(childdata);
  };
  //end

  //resetBtn start
  const resetBtn = () => {
    setTagStatus(null);
    setcategoryVal(null);
    setdeleteMsg(true);
  };
  //resetBtn end

  //permission update Btn start
  const permissionUpdateBtn = () => {
    if (categoryVal === "" || permissions.length < 0) {
      Swal.fire({
        position: "top-end",
        icon: "warning",
        title: "All Field Requried!!",
        showConfirmButton: false,
        timer: 1500,
      });
    } else {
      Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/insertPermission`, {
        Category: categoryVal,
        Permission: permissions,
      }).then((response) => {
        setTagStatus(null);
        setcategoryVal(null);
        Swal.fire({
          position: "top-end",
          icon: "success",
          title: "Your work has been saved",
          showConfirmButton: false,
          timer: 1500,
        });
      });
    }
  };
  //permission update Btn end

  //remove permissions in tagspickker start
  const permissionsRemove = (e) => {
    Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/removePermission`, {
      categoryVal: categoryVal,
      deletePermissionID: e,
    }).then((response) => {
      if (response.data === "deleted") {
        setdeleteMsg(false);
        const timer = setTimeout(() => {
          setdeleteMsg(true);
        }, 2000);
        return () => clearTimeout(timer);
      }
    });
  };
  //remove permissions in tagspickker end

  return (
    <React.Fragment>
      <Modal.Header>
        <Modal.Title className="tittle_modal">
          <FontAwesomeIcon icon={faMagic} className="icon_modal" />
          Update Details
        </Modal.Title>
      </Modal.Header>
      <Divider />
      <Modal.Body className="permissionModal">
        <Grid fluid={true}>
          <Form fluid={true}>
            <Row className="show-grid">
              <Form.Group controlId="name-1" className="m-2">
                <Form.ControlLabel>Category</Form.ControlLabel>

                <PermissiontTypePicker
                  onChange={displayPermissions}
                  value={categoryVal}
                />
              </Form.Group>
            </Row>
            <Row className="show-grid">
              <Form.Group controlId="email-1" className="m-2">
                <Form.ControlLabel>
                  Permission(s){" "}
                  <span hidden={deleteMsg} style={{ color: "red" }}>
                    **Deleted**
                  </span>
                </Form.ControlLabel>

                <PermissionTagsPicker
                  childToParent={childToParent}
                  categoryVal={categoryVal}
                  permissionRemove={permissionsRemove}
                />
              </Form.Group>
            </Row>
          </Form>
        </Grid>
      </Modal.Body>
      <Modal.Footer className="btnGroupmodal">
        <PrimaryBtn onClick={permissionUpdateBtn} value="Update" />
        <SecondaryBtn onClick={resetBtn} value="Reset" />
      </Modal.Footer>
    </React.Fragment>
  );
}
