import React, { useState } from "react";
import $ from "jquery";
import Axios from "axios";
import Swal from "sweetalert2";
import "./modalPermissionAssign.css";
import { Modal, Grid, Row, Panel } from "rsuite";
import PrimaryBtn from "../../buttons/primaryBtn";
import SecondaryBtn from "../../buttons/secondaryBtn";
import TablePermission from "../../Tables/tablePermissions";
import PermissionTypePicker from "../../inputFields/permissionTypePicker";
import PermissionsPicker from "../../inputFields/permissionsPicker";

export default function ModalPermissionAssign(props) {
  const [categoryVal, setcategoryVal] = useState();
  const [listOpen, setListOpen] = useState(true);
  const [refreshKey, setRefreshKey] = useState(0);
  const [permissions, setPermissions] = useState([]);

  const HOST = `${process.env.REACT_APP_HOST}`;
  const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
  const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

  //show permissions for selected category and show permission list start
  const displayPermissions = (e) => {
    setcategoryVal(e);
    setListOpen(false);
  };
  //end

  //get selected permissions from child component start
  const childToParent = (childdata) => {
    setPermissions(childdata);
  };
  //end

  //assignBtn start
  const assignPermissionsBtn = () => {
    if (permissions.length === 0) {
      Swal.fire({
        position: "top-end",
        icon: "warning",
        title: "All Field Requried!",
        showConfirmButton: false,
        timer: 1500,
      });
    } else {
      Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/assignPermission`, {
        roleID: props.data,
        PermissionID: permissions,
      }).then(() => {
        console.log("Assign permission success");
      });
      //send new 'key'for table, to refresh new data after assign completed
      setRefreshKey((key) => key + 1);
      //end
    }
  };
  //assignBtn end

  //resetBtn start
  const resetBtn = () => {
    setcategoryVal("");
    setPermissions([]);
  };
  //resetBtn end

  //assigned permission remove start
  const permissionRemoveBtn = () => {
    let deleteID = [];
    deleteID = $('input[name="removePermissionId"]:checked')
      .map(function () {
        return $(this).val();
      })
      .get();
    console.log(deleteID);
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#079577",
      cancelButtonColor: "#fd7e14",

      confirmButtonText: "Yes, delete!",
    }).then((result) => {
      if (result.isConfirmed) {
        Axios.post(
          `${PROTOCOL}://${HOST}:${SERVER_PORT}/assignedPermissionRemove`,
          {
            deleteID: deleteID,
            roleID: props.data,
          }
        ).then((result) => {
          //  setdeleteID(result.data);
        });
        // Swal.fire("Deleted!", "Your Record has been deleted.", "success");
        //send new 'key'for table, to refresh new data after remove completed
        setRefreshKey((key) => key + 2);
        //end
      }
    });
  };
  //assigned permission remove end

  return (
    <React.Fragment>
      <Modal.Header>
        <Modal.Title className="tittle_modal">Assign Permission(s)</Modal.Title>
      </Modal.Header>
      <Modal.Body className="mainBody">
        <Grid fluid={true} className="mainContent">
          <Panel header="Add New Permission(s)" bordered className="mt-2">
            <Row className="show-grid">
              <PermissionTypePicker
                onChange={displayPermissions}
                value={categoryVal}
                Placeholder="Select category here..."
              />
            </Row>
            <Row className="show-grid mt-3" hidden={listOpen}>
              <PermissionsPicker
                categoryVal={categoryVal}
                childToParent={childToParent}
                placement="bottomStart"
                // style={{ width: "540px" }}
              />
            </Row>
          </Panel>

          <Row className="mt-3">
            <TablePermission
              onClick={permissionRemoveBtn}
              RoleID={props.data}
              key={refreshKey}
              removePermissionId="removePermissionId"
            />
          </Row>
        </Grid>
      </Modal.Body>
      <Modal.Footer className="btnGroupmodal">
        <PrimaryBtn value="Submit" onClick={assignPermissionsBtn} />
        <SecondaryBtn value="Reset" onClick={resetBtn} />
      </Modal.Footer>
    </React.Fragment>
  );
}
