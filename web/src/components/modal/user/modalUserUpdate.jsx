import React, { useState, useEffect } from "react";
import {
  Modal,
  Panel,
  Grid,
  Row,
  Tooltip,
  Input,
  Whisper,
  Col,
  InputGroup,
  Form,
} from "rsuite";
import {
  faUser,
  faUserLock,
  faUserEdit,
  faEnvelope,
} from "@fortawesome/free-solid-svg-icons";
import { Animated } from "react-animated-css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Axios from "axios";
import $ from "jquery";
import "./modalUserUpdate.css";
import PrimaryBtn from "../../buttons/primaryBtn";
import SecondaryBtn from "../../buttons/secondaryBtn";
import PswField from "../../inputFields/pswField";
import ChannelPicker from "../../inputFields/channelPicker";
import HiddenInputField from "../../inputFields/hiddenInputField";
import StatusPicker from "../../inputFields/statusPicker";
import RolePicker from "../../inputFields/rolePicker";
import Swal from "sweetalert2";

export default function ModalUserUpdate({ parentToChild, reloadTable }) {
  const [userData, setuserData] = useState([]);
  const [status, setstatus] = useState("");
  const [statusVal, setStatusVal] = useState();
  const [channel, setChannel] = useState("");
  const [channelVal, setChannelVal] = useState();
  const [roleUserValue, setRoleUserValue] = useState([]);
  const [userRoleList, setUserRoleList] = useState([]);

  const HOST = `${process.env.REACT_APP_HOST}`;
  const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
  const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

  useEffect(() => {
    //load user details according to selected user id start
    let isMounted = true; // note mutable flag
    Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/getUserData_ID`, {
      idVal: parentToChild,
    }).then((result) => {
      if (isMounted) setuserData(result.data);
    });
    //load user details according to selected user id end

    //load assigned roles of selected user id start
    Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/getUserRoleList`, {
      idVal: parentToChild,
    }).then((resultt) => {
      setUserRoleList(resultt.data);
    });
    return () => {
      isMounted = false;
    }; // cleanup toggles value, if unmounted
    //load assigned roles of selected user id end
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  //display assigned roles of selected user on default in the modal 'rolePicker' input field start
  useEffect(() => {
    const map = Array.prototype.map;
    let newName = [];
    newName = map.call(userRoleList, (eachLetter) => {
      return eachLetter.role_id;
    });
    setRoleUserValue(newName);
  }, [userRoleList]);
  //display assigned roles of selected user in default in the modal 'rolePicker' input field end

  //resetBtn
  const resetBtn = () => {
    setChannelVal(null);
    setStatusVal(null);

    setRoleUserValue([]);
    $('input[name="inputFields"]').val("");
  };
  //resetBtn end

  //user updateBtn start
  const updateUserBtn = () => {
    if (
      $("#fnVal").val() === "" ||
      $("#lnVal").val() === "" ||
      $("#emailVal").val() === "" ||
      $("#statusInput").val() === "0" ||
      $("#channelInput").val() === "0" ||
      $("#pswVal").val() === "" ||
      roleUserValue.length === 0
    ) {
      Swal.fire({
        position: "top-end",
        icon: "warning",
        title: "All Field Requried!",
        showConfirmButton: false,
        timer: 1500,
      });
    } else {
      Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/updateUser`, {
        fn: $("#fnVal").val(),
        ln: $("#lnVal").val(),
        status: $("#statusInput").val(),
        channel: $("#channelInput").val(),
        email: $("#emailVal").val(),
        password: $("#pswVal").val(),
        role: roleUserValue,
        idVal: parentToChild,
      }).then(() => {
        reloadTable();
      });
    }
  };

  //user updateBtn end

  return (
    <React.Fragment>
      <Modal.Header>
        <Modal.Title>
          <div className="modalHeading">
            <FontAwesomeIcon icon={faUserEdit} /> User Info
          </div>

          <span className="tagline">Fill out the below form.</span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {/* <PanelGroup> */}
        <Panel header="" shaded className="panel1">
          <Animated
            animationIn="zoomInLeft"
            animationOut="fadeOut"
            isVisible={true}
          >
            <Grid fluid={true}>
              {userData.map((val, key) => {
                return (
                  <React.Fragment>
                    <Row className="show-grid">
                      <Col xs={12} xsPush={12} md={12}>
                        <label>Email:</label>{" "}
                        <InputGroup className="InputGroup">
                          <InputGroup.Addon>
                            <FontAwesomeIcon icon={faEnvelope} />
                          </InputGroup.Addon>

                          <Whisper
                            trigger="hover"
                            speaker={<Tooltip>Required</Tooltip>}
                          >
                            <Input
                              id="emailVal"
                              key="1"
                              placeholder="Email@abc.com"
                              type="text"
                              name="inputFields"
                              defaultValue={val.email}
                            />
                          </Whisper>
                        </InputGroup>
                        <label>Password:</label>
                        <InputGroup inside className="InputGroup">
                          <PswField key="2" defaultValue={val.password} />
                        </InputGroup>
                        <label>Status:</label>
                        <Form.Group controlId="radioList">
                          <HiddenInputField
                            name="inputFields"
                            id="statusInput"
                            key="3"
                            value={status === "" ? val.status : status}
                          />
                          <StatusPicker
                            value={statusVal}
                            id="statusVal"
                            key="4"
                            style={{ width: 300 }}
                            onClean={() => {
                              setstatus("0");
                            }}
                            onChange={(event) => {
                              setstatus(event);
                              setStatusVal();
                            }}
                            defaultValue={
                              val.status === "Active" ? "Active" : "Inactive"
                            }
                          />
                        </Form.Group>
                      </Col>

                      <Col xs={12} xsPull={12}>
                        <label>Name:</label>
                        <InputGroup className="InputGroup">
                          <InputGroup.Addon>
                            <FontAwesomeIcon icon={faUser}></FontAwesomeIcon>
                          </InputGroup.Addon>
                          <Whisper
                            trigger="hover"
                            speaker={<Tooltip>Required</Tooltip>}
                          >
                            <Input
                              style={{ width: 200 }}
                              placeholder="Name"
                              type="text"
                              id="fnVal"
                              key="5"
                              name="inputFields"
                              defaultValue={val.fn}
                            />
                          </Whisper>
                        </InputGroup>
                        <label>Username:</label>
                        <InputGroup className="InputGroup">
                          <InputGroup.Addon>
                            <FontAwesomeIcon
                              icon={faUserLock}
                            ></FontAwesomeIcon>
                          </InputGroup.Addon>
                          <Whisper
                            trigger="hover"
                            speaker={<Tooltip>Required</Tooltip>}
                          >
                            <Input
                              style={{ width: 200 }}
                              placeholder="Username"
                              type="text"
                              id="lnVal"
                              key="6"
                              name="inputFields"
                              defaultValue={val.ln}
                            />
                          </Whisper>
                        </InputGroup>
                        <label>Assigned Channel(s):</label>
                        <Form.Group controlId="channel">
                          <HiddenInputField
                            name="inputFields"
                            id="channelInput"
                            key="7"
                            value={channel === "" ? val.idchannel : channel}
                          />
                          <ChannelPicker
                            style={{ width: 300 }}
                            value={channelVal}
                            onClean={() => {
                              setChannel("0");
                            }}
                            onChange={(event) => {
                              setChannel(event);
                              setChannelVal();
                            }}
                            defaultValue={val.idchannel}
                            id="channelVal"
                            key="9"
                            menuMaxHeight={250}
                            placement="topStart"
                          />
                        </Form.Group>
                      </Col>
                    </Row>
                    <Row className="show-grid mt-2">
                      <Col>
                        <label>Assigned role(s):</label>
                        <Form.Group controlId="channel">
                          <RolePicker
                            onChange={setRoleUserValue}
                            value={roleUserValue.map((val, kk) => val)}
                            style={{ width: 510 }}
                            key="8"
                            menuMaxHeight={250}
                            placement="topStart"
                          />
                        </Form.Group>
                      </Col>
                    </Row>
                  </React.Fragment>
                );
              })}
            </Grid>
          </Animated>
        </Panel>
        {/*           
        </PanelGroup> */}
      </Modal.Body>
      <Modal.Footer className="btnGroupmodal">
        <PrimaryBtn onClick={updateUserBtn} value="Update" />
        <SecondaryBtn onClick={resetBtn} value="Reset" />
      </Modal.Footer>
    </React.Fragment>
  );
}
