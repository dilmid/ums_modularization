import React, { useState, useEffect } from "react";
import { TagInput } from "rsuite";
import Axios from "axios";

export default function PermissionTagsPicker({
  permissionRemove,
  categoryVal,
  onClose,
  childToParent,
}) {
  const [tagStatus, setTagStatus] = useState([]);
  const [permisssionList, setPermisssionList] = useState([]);

  const HOST = `${process.env.REACT_APP_HOST}`;
  const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
  const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

  useEffect(() => {
    Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/getPermission_name`, {
      pCategory: categoryVal,
    }).then((response) => {
      setPermisssionList(response.data);
    });
  }, [categoryVal]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const map = Array.prototype.map;
    let newName = [];
    newName = map.call(permisssionList, (eachLetter) => {
      return `${eachLetter.permission_name}`;
    });

    setTagStatus(newName);
  }, [permisssionList]);

  const clearTags = (e) => {
    permissionRemove(e);
    setTagStatus(e);
    // console.log(tagStatus);
  };

  return (
    <React.Fragment>
      <TagInput
        block
        placeholder="Add new permission(s)"
        value={tagStatus}
        onClose={onClose}
        onCreate={(value) => {
          setTagStatus(value);
          childToParent(value);
        }}
        onChange={clearTags}
        defaultValue={
          tagStatus === null ? "" : tagStatus.map((val, kk) => `${val}`)
        }
        trigger={["Enter", "Space", "Comma"]}
      />
      <span className="infoLine">
        Type and Enter/Comma/Space for add new permission
      </span>
    </React.Fragment>
  );
}
