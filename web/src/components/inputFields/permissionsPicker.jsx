import React, { useState, useEffect, useRef } from "react";
import Axios from "axios";
import { CheckPicker } from "rsuite";

export default function PermissionsPicker({
  style,
  categoryVal,
  childToParent,
  placement,
}) {
  const [permissions, setPermissions] = useState([]);
  const picker = useRef();
  const [value, setValue] = useState([]);

  // console.log(categoryVal);

  useEffect(() => {
    const HOST = `${process.env.REACT_APP_HOST}`;
    const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
    const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

    Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/getPermission_name`, {
      pCategory: categoryVal,
    }).then((response) => {
      setPermissions(response.data);
    });
  }, [categoryVal]);

  //checkPicker onchange event
  const handleChange = (value) => {
    setValue(value);
    childToParent(value);
  
  };
  //checkPicker onchange event end

  return (
    <React.Fragment>
      <div className="example-item">
        <CheckPicker
          placement={placement}
          block
          data={permissions.map((value) => {
            return {
              label: value.permission_name,
              value: value.permission_type_id,
            };
          })}
          placeholder="Select permission(s)"
          ref={picker}
          value={value}
          onChange={handleChange}
          style={style}
        />
      </div>
    </React.Fragment>
  );
}
