import React, { useState, useEffect } from "react";
import Axios from "axios";
import { CheckPicker } from "rsuite";

export default function RolePicker({
  style,
  placement,
  menuMaxHeight,
  onChange,
  value,
}) {
  const [roleList, setroleList] = useState([]);

  useEffect(() => {
    const HOST = `${process.env.REACT_APP_HOST}`;
    const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
    const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

    Axios.get(`${PROTOCOL}://${HOST}:${SERVER_PORT}/getRoleList`).then(
      (response) => {
        setroleList(response.data);
      }
    );
  }, []);
  return (
    <React.Fragment>
      <CheckPicker
        sticky
        data={roleList.map((x) => {
          return {
            label: x.role_name,
            value: x.role_id,
          };
        })}
        placement={placement}
        placeholder="Assign role(s) to user"
        onChange={onChange}
        value={value}
        block
        menuMaxHeight={menuMaxHeight}
        style={style}
      />
    </React.Fragment>
  );
}
