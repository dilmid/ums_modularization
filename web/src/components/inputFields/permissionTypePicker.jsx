import React, { useState, useEffect } from "react";
import Axios from "axios";
import { SelectPicker } from "rsuite";

export default function PermissionTypePicker({
  Placeholder,
  id,
  onChange,
  placement,
  value,
}) {
  const [permissionType, setPermissionType] = useState([]);

  useEffect(() => {
    const HOST = `${process.env.REACT_APP_HOST}`;
    const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
    const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

    Axios.get(`${PROTOCOL}://${HOST}:${SERVER_PORT}/getPermission`).then(
      (response) => {
        setPermissionType(response.data);
      }
    );
  }, []);

  return (
    <React.Fragment>
      <SelectPicker
        data={permissionType.map((val) => {
          return {
            label: val.permission_category,
            value: val.permission_category,
          };
        })}
        block
        onChange={onChange}
        // onSelect={onSelect}
        placeholder={Placeholder}
        placement={placement}
        value={value}
        id={id}
      />
    </React.Fragment>
  );
}
