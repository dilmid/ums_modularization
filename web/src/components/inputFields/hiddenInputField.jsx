import React from "react";

export default function HiddenInputField({ value, name, id }) {
  return (
    <React.Fragment>
      <input
        type="text"
        style={{
          visibility: "hidden",
          position: "absolute",
        }}
        name={name}
        readOnly
        id={id}
        value={value}
      />
    </React.Fragment>
  );
}
