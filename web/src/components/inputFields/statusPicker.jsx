import React from "react";
import { SelectPicker } from "rsuite";
export default function StatusPicker({
  onClean,
  onClose,
  style,
  id,
  defaultValue,
  onChange,
  value,
}) {
  //user status types
  const options = [
    { value: "Active", label: "Active" },
    { value: "Inactive", label: "Inactive" },
  ];
  //user status types end

  return (
    <React.Fragment>
      <SelectPicker
        data={options}
        value={value}
        block
        id={id}
        style={style}
        appearance="default"
        placeholder="Status"
        searchable={false}
        defaultValue={defaultValue}
        onChange={onChange}
        onClose={onClose}
        onClean={onClean}
      />
    </React.Fragment>
  );
}
