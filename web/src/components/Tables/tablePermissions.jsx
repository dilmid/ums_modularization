import React, { useState, useEffect } from "react";
import { Animated } from "react-animated-css";
import Axios from "axios";
import { Table, Pagination, Panel, Whisper, Tooltip } from "rsuite";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";

export default function TablePermissions({
  removePermissionId,
  RoleID,
  onClick,
  tableID,
  Key,
}) {
  const [permissionsList, setPermissionsList] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(1);

  //load permission details according to seleted role id start
  useEffect(() => {
    const HOST = `${process.env.REACT_APP_HOST}`;
    const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
    const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

    Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/getPermission_roleID`, {
      RoleID: RoleID,
    }).then((response) => {
      setPermissionsList(response.data);
    });
  }, [RoleID]);
  //load permission details according to seleted role id end

  //table pagination start
  const permissionData = permissionsList.filter((v, i) => {
    const start = limit * (page - 1);
    const end = start + limit;
    return i >= start && i < end;
  });
  //table pagination end

  // set number of rows for one page in the table start (10 or 20 rows)
  const handleChangeLimit = (dataKey) => {
    setPage(1);
    setLimit(dataKey);
  };
  // set number of rows for one page in the table end (10 or 20 rows)

  //table css for headers
  const tableStyles = {
    header: {
      backgroundColor: "#079577",
      letterSpacing: "1.5px",
      fontSize: "14px",
      fontWeight: "700",
      color: "white",
      opacity: "0.8",
      textShadow: "2px 4px 3px rgba(0,0,0,0.3)",
    },
    borderLeft: {
      borderRadius: "5px 0px 0px 0px",
    },
    borderRight: {
      borderRadius: "0px 5px 0px 0px",
    },
  };
  //table css for headers end

  return (
    <React.Fragment>
      <Panel header="Assigned Permission(s)" bordered className="mt-2">
        <Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>
          <Table
            height={350}
            data={permissionData}
            // loading={true}
            className="permissionTable"
            key={Key}
            id={tableID}
          >
            <Table.Column width={80} align="center" fixed>
              <Table.HeaderCell
                style={Object.assign(
                  {},
                  tableStyles.header,
                  tableStyles.borderLeft
                )}
              >
                #
              </Table.HeaderCell>

              <Table.Cell
                className="checkboxBorder"
                dataKey="permission_type_id"
              >
                {(rowData) => {
                  return (
                    <Whisper
                      placement="topStart"
                      controlId="control-id-hover"
                      trigger="hover"
                      speaker={<Tooltip>Remove</Tooltip>}
                    >
                      <span className="ActionRemove" onClick={onClick}>
                        <input
                          name={removePermissionId}
                          defaultValue={`${rowData.permission_type_id}`}
                          type="checkbox"
                          id={`${rowData.permission_type_id}`}
                          className="radioImg2"
                        />
                        <label
                          htmlFor={`${rowData.permission_type_id}`}
                          className="radioImg2Label"
                        >
                          <FontAwesomeIcon
                            icon={faTrash}
                            className="fontIcon RemoveIcon"
                          />
                        </label>
                      </span>
                    </Whisper>
                  );
                }}
              </Table.Cell>
            </Table.Column>
            <Table.Column width={160} fixed>
              <Table.HeaderCell
                className="header__cell"
                style={Object.assign({}, tableStyles.header)}
              >
                Category
              </Table.HeaderCell>
              <Table.Cell className="TableCell" dataKey="permission_category" />
            </Table.Column>
            <Table.Column width={160} fixed>
              <Table.HeaderCell
                className="header__cell"
                style={Object.assign({}, tableStyles.header)}
              >
                Name
              </Table.HeaderCell>
              <Table.Cell className="TableCell" dataKey="permission_name" />
            </Table.Column>

            <Table.Column width={100}>
              <Table.HeaderCell style={Object.assign({}, tableStyles.header)}>
                Status
              </Table.HeaderCell>
              <Table.Cell className="TableCell" dataKey="pstatus" />
            </Table.Column>
            <Table.Column width={250}>
              <Table.HeaderCell
                style={Object.assign(
                  {},
                  tableStyles.header,
                  tableStyles.borderRight
                )}
              >
                Assigned on
              </Table.HeaderCell>
              <Table.Cell className="TableCell" dataKey="pdate" />
            </Table.Column>
          </Table>
          <div style={{ padding: 20 }}>
            <Pagination
              prev
              next
              first
              last
              ellipsis
              boundaryLinks
              maxButtons={5}
              size="xs"
              layout={["total", "-", "limit", "|", "pager", "skip"]}
              total={permissionsList.length}
              limitOptions={[10, 20]}
              limit={limit}
              activePage={page}
              onChangePage={setPage}
              onChangeLimit={handleChangeLimit}
            />
          </div>
        </Animated>
      </Panel>
    </React.Fragment>
  );
}
