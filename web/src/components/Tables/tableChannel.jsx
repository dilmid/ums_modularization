import React, { useEffect, useState } from "react";
import "./table.css";
import Axios from "axios";
import { Animated } from "react-animated-css";
import { Table, Pagination, Whisper, Tooltip } from "rsuite";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faEdit } from "@fortawesome/free-solid-svg-icons";

export default function TableChannel({
  removeChannelId,
  reloadTable2,
  editClick,
  editName,
  editClassName,
}) {
  const [channelDataList, setChannelDataList] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(1);

  //if channel table got update, show only update row or else show all channels in the table start
  useEffect(() => {
    const HOST = `${process.env.REACT_APP_HOST}`;
    const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
    const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

    if (reloadTable2 > 0) {
      Axios.get(
        `${PROTOCOL}://${HOST}:${SERVER_PORT}/getChannelList_Modified`
      ).then((response) => {
        setChannelDataList(response.data);
      });
    } else {
      Axios.get(`${PROTOCOL}://${HOST}:${SERVER_PORT}/getChannelList`).then(
        (response) => {
          setChannelDataList(response.data);
        }
      );
    }
  }, [reloadTable2]);
  //if channel table got update show only update row or else show all channels in the table end

  //table pagination start
  const channelData = channelDataList.filter((v, i) => {
    const start = limit * (page - 1);
    const end = start + limit;
    return i >= start && i < end;
  });

  // table pagination end

  // set number of rows for one page in the table start (10 or 20 rows)
  const handleChangeLimit = (dataKey) => {
    setPage(1);
    setLimit(dataKey);
  };
  // set number of rows for one page in the table end (10 or 20 rows)

  //table css for headers
  const tableStyles = {
    header: {
      backgroundColor: "#079577",
      letterSpacing: "1.5px",
      fontSize: "14px",
      fontWeight: "700",
      color: "white",
      opacity: "0.8",
      textShadow: "2px 4px 3px rgba(0,0,0,0.3)",
    },
    borderLeft: {
      borderRadius: "5px 0px 0px 0px",
    },
    borderRight: {
      borderRadius: "0px 5px 0px 0px",
    },
  };
  //table css for headers end

  return (
    <React.Fragment>
      <Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>
        <Table
          height={350}
          data={channelData}
          loading={false}
          className="roleTable"
          id="datafilter"
        >
          <Table.Column width={60} align="center" fixed>
            <Table.HeaderCell
              style={Object.assign(
                {},
                tableStyles.header,
                tableStyles.borderLeft
              )}
            >
              #
            </Table.HeaderCell>

            <Table.Cell className="checkboxBorder" dataKey="idchannel" />
          </Table.Column>

          <Table.Column width={150} fixed>
            <Table.HeaderCell
              className="header__cell"
              style={Object.assign({}, tableStyles.header)}
            >
              Channel
            </Table.HeaderCell>
            <Table.Cell className="TableCell" dataKey="channel_name" />
          </Table.Column>

          <Table.Column width={150}>
            <Table.HeaderCell style={Object.assign({}, tableStyles.header)}>
              Description
            </Table.HeaderCell>
            <Table.Cell className="TableCell" dataKey="channel_desc" />
          </Table.Column>

          <Table.Column width={250}>
            <Table.HeaderCell style={Object.assign({}, tableStyles.header)}>
              Created
            </Table.HeaderCell>
            <Table.Cell className="TableCell" dataKey="channel_date" />
          </Table.Column>
          <Table.Column width={60} flexGrow={1}>
            <Table.HeaderCell style={Object.assign({}, tableStyles.header)}>
              Status
            </Table.HeaderCell>
            <Table.Cell className="TableCell" dataKey="channel_status" />
          </Table.Column>
          <Table.Column width={150} fixed="right">
            <Table.HeaderCell
              style={Object.assign(
                {},
                tableStyles.header,
                tableStyles.borderRight
              )}
            >
              Action
            </Table.HeaderCell>

            <Table.Cell className="TableCell">
              {(rowData) => {
                return (
                  <React.Fragment>
                    <Whisper
                      placement="leftStart"
                      controlId="control-id-hover"
                      trigger="hover"
                      speaker={<Tooltip>Edit</Tooltip>}
                    >
                      <span className="Action">
                        <input
                          type="Radio"
                          onClick={editClick}
                          className={editClassName}
                          name={editName}
                          readOnly
                          value={`${rowData.idchannel}`}
                        />
                        <FontAwesomeIcon
                          icon={faEdit}
                          className="fontIcon"
                        />
                      </span>
                    </Whisper>
                    |
                    <Whisper
                      placement="rightStart"
                      controlId="control-id-hover"
                      trigger="hover"
                      speaker={<Tooltip>Remove</Tooltip>}
                    >
                      <span className="ActionRemove">
                        <input
                          name={removeChannelId}
                          defaultValue={`${rowData.idchannel}`}
                          type="checkbox"
                          id={`${rowData.idchannel}`}
                          className="radioImg2"
                        />
                        <label
                          htmlFor={`${rowData.idchannel}`}
                          className="radioImg2Label"
                        >
                          <FontAwesomeIcon
                            icon={faTrash}
                            className="fontIcon RemoveIcon"
                          />
                        </label>
                      </span>
                    </Whisper>
                  </React.Fragment>
                );
              }}
            </Table.Cell>
          </Table.Column>
        </Table>
        <div style={{ padding: 20 }}>
          <Pagination
            prev
            next
            first
            last
            ellipsis
            boundaryLinks
            maxButtons={5}
            size="xs"
            layout={["total", "-", "limit", "|", "pager", "skip"]}
            total={channelDataList.length}
            limitOptions={[10, 20]}
            limit={limit}
            activePage={page}
            onChangePage={setPage}
            onChangeLimit={handleChangeLimit}
          />
        </div>
      </Animated>
    </React.Fragment>
  );
}
