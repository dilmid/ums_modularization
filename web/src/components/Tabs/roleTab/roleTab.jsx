import React, { useState } from "react";
import TableRole from "../../Tables/tableRole";
import {
  Grid,
  Row,
  Col,
  Modal,
  Whisper,
  Tooltip,
  ButtonToolbar,
  IconButton,
} from "rsuite";
import CreateBtn from "../../buttons/createBtn";
import { DeleteOutline, Cached, AdminPanelSettings } from "@mui/icons-material";
import RemoveBtn from "../../buttons/removeBtn";
import $ from "jquery";
import Swal from "sweetalert2";
import Axios from "axios";
import ModalRoleNew from "../../modal/role/modalRoleNew";
import ModalRoleUpdate from "../../modal/role/modalRoleUpdate";
import "../../Tables/table.css";

export default function RoleTab() {
  const [modalRoleNew, setmodalRoleNew] = useState(false);
  const [reloadTable2, setreloadTable2] = useState(0);
  const [modalRoleUpdate, setmodalRoleUpdate] = useState(false);
  const [roleID, setroleID] = useState();

  //change status of selected roles (deleteBTn) start
  const roleMultiDeleteBtn = () => {
    const HOST = `${process.env.REACT_APP_HOST}`;
    const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
    const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

    let deleteID = [];
    deleteID = $('input[name="removeRoleId"]:checked')
      .map(function () {
        return $(this).val();
      })
      .get();

    if (deleteID.length === 0) {
      let timerInterval;
      Swal.fire({
        title: "Select Record(s)",
        html: "Please! select record(s) to delete.",
        timer: 3000,
        timerProgressBar: true,
        showConfirmButton: false,
        icon: "error",
        willClose: () => {
          clearInterval(timerInterval);
        },
      }).then((result) => {});
    } else {
      Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#079577",
        cancelButtonColor: "#fd7e14",

        confirmButtonText: "Yes, delete all!",
      }).then((result) => {
        if (result.isConfirmed) {
          Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/roleRemoveMulti`, {
            deleteID: deleteID,
          }).then((result) => {
            //  setdeleteID(result.data);
          });
          Swal.fire("Deleted!", "Your Record has been deleted.", "success");
          setreloadTable2((key) => key + 3);
        }
      });
    }
  };
  //change status of selected roles (deleteBTn) end

  //open new role insert modal start
  const roleNewBtn = () => {
    setmodalRoleNew(true);
  };
  //open new role insert modal end

  //close new role insert modal start
  const modalRoleNewClose = () => setmodalRoleNew(false);
  //close new role insert modal end

  //open role update modal start
  const showUpdateRoleModal = () => {
    $('input[name="Radios"]:checked').each(function () {
      setroleID(this.value);

      $(".radioImg").prop("checked", false);
      $(this).prop("checked", true);

      setmodalRoleUpdate(true);
    });
  };
  //open role update modal endclose

  //close role update modal start
  const modalRoleUpdateClose = () => setmodalRoleUpdate(false);
  //close role update modal end

  //get new function from child component start
  const reloadTable = () => {
    setreloadTable2((key) => key + 2);
  };
  //get new function from child component end

  //table refresh Btn start
  const refreshBtn = () => {
    setreloadTable2(0);
  };
  //table refresh Btn end

  return (
    <React.Fragment>
      <div className="modal-container">
        <Modal size="sm" open={modalRoleNew} onClose={modalRoleNewClose}>
          <ModalRoleNew reloadTable={reloadTable} />
        </Modal>
        <Modal size="sm" open={modalRoleUpdate} onClose={modalRoleUpdateClose}>
          <ModalRoleUpdate parentToChild={roleID} reloadTable={reloadTable} />
        </Modal>
      </div>

      <Grid fluid className="mb-4">
        <Row className="show-grid">
          <Col xs={6} style={{ display: "inline-flex" }}>
            <CreateBtn
              startIcon={<AdminPanelSettings />}
              className="createBtn"
              onClick={roleNewBtn}
            />
            <RemoveBtn
              startIcon={<DeleteOutline />}
              className="removeBtn"
              onClick={roleMultiDeleteBtn}
            />
          </Col>
          <Col xs={6} xsPush={12} style={{ paddingLeft: "160px" }}>
            <Whisper
              placement="top"
              controlId="control-id-hover"
              trigger="hover"
              speaker={<Tooltip>Table Refresh</Tooltip>}
            >
              <ButtonToolbar>
                <IconButton
                  icon={<Cached />}
                  size="xs"
                  appearance="primary"
                  circle
                  className="refresh_icon"
                  onClick={refreshBtn}
                />
              </ButtonToolbar>
            </Whisper>
          </Col>
        </Row>
      </Grid>
      <TableRole
        removeRoleId="removeRoleId"
        editClick={showUpdateRoleModal}
        editName="Radios"
        reloadTable2={reloadTable2}
        editClassName="radioImg"
      />
    </React.Fragment>
  );
}
