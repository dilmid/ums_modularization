import React, { useState } from "react";
import TableUser from "../../Tables/tableUser";
import {
  Grid,
  Row,
  Col,
  Modal,
  ButtonToolbar,
  IconButton,
  Whisper,
  Tooltip,
} from "rsuite";
import CreateBtn from "../../buttons/createBtn";
import PersonAddOutlinedIcon from "@mui/icons-material/PersonAddOutlined";
import { DeleteOutline, Cached } from "@mui/icons-material";
import RemoveBtn from "../../buttons/removeBtn";
import $ from "jquery";
import Swal from "sweetalert2";
import Axios from "axios";
import ModalUserNew from "../../modal/user/modalUserNew";
import "../../Tables/table.css";
import ModalUserUpdate from "../../modal/user/modalUserUpdate";

export default function UserTab() {
  const [modalUserNew, setmodalUserNew] = useState(false);
  const [userID, setuserID] = useState("");

  const [reloadTable2, setreloadTable2] = useState(0);
  const [modalUserUpdate, setmodalUserUpdate] = useState(false);

  const modalUserNewClose = () => setmodalUserNew(false);

  const userMultiDeleteBtn = () => {
    const HOST = `${process.env.REACT_APP_HOST}`;
    const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
    const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;
    let deleteID = [];
    deleteID = $('input[name="removeUserId"]:checked')
      .map(function () {
        return $(this).val();
      })
      .get();
    // setRole(searchIDs);
    // console.log(deleteID);
    if (deleteID.length === 0) {
      let timerInterval;
      Swal.fire({
        title: "Select Record(s)",
        html: "Please! select record(s) to delete.",
        timer: 3000,
        showConfirmButton: false,
        timerProgressBar: true,
        icon: "error",
        willClose: () => {
          clearInterval(timerInterval);
        },
      }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
          console.log("I was closed by the timer");
        }
      });
    } else {
      Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#079577",
        cancelButtonColor: "#fd7e14",

        confirmButtonText: "Yes, delete all!",
      }).then((result) => {
        if (result.isConfirmed) {
          Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/userRemoveMulti`, {
            deleteID: deleteID,
          }).then((result) => {
            //  setdeleteID(result.data);
          });
          Swal.fire("Deleted!", "Your Record has been deleted.", "success");
          setreloadTable2((key) => key + 4);
        }
      });
    }
  };

  const userNewBtn = () => {
    setmodalUserNew(true);
  };

  const reloadTable = () => {
    // alert("user tab");
    setreloadTable2((key) => key + 3);
  };

  const refreshBtn = () => {
    setreloadTable2(0);
  };

  const showUpdateUserModal = () => {
    // alert("ok");
    $('input[name="Radios"]:checked').each(function () {
      //  console.log(this.value);
      setuserID(this.value);

      $(".radioImg").prop("checked", false);
      $(this).prop("checked", true);

      setmodalUserUpdate(true);
    });
  };

  const modalUserUpdateClose = () => setmodalUserUpdate(false);

  return (
    <React.Fragment>
      <div className="modal-container">
        <Modal size="sm" open={modalUserNew} onClose={modalUserNewClose}>
          <ModalUserNew reloadTable={reloadTable} />
        </Modal>

        <Modal size="sm" open={modalUserUpdate} onClose={modalUserUpdateClose}>
          <ModalUserUpdate parentToChild={userID} reloadTable={reloadTable} />
        </Modal>
      </div>

      <Grid fluid>
        <Row className="show-grid">
          <Col xs={6} style={{ display: "inline-flex" }}>
            <CreateBtn
              startIcon={<PersonAddOutlinedIcon />}
              className="createBtn"
              onClick={userNewBtn}
            />
            <RemoveBtn
              startIcon={<DeleteOutline />}
              className="removeBtn"
              onClick={userMultiDeleteBtn}
            />
          </Col>
          <Col xs={6} xsPush={12} style={{ paddingLeft: "160px" }}>
            <Whisper
              placement="top"
              controlId="control-id-hover"
              trigger="hover"
              speaker={<Tooltip>Table Refresh</Tooltip>}
            >
              <ButtonToolbar>
                <IconButton
                  icon={<Cached />}
                  size="xs"
                  appearance="primary"
                  circle
                  className="refresh_icon"
                  onClick={refreshBtn}
                />
              </ButtonToolbar>
            </Whisper>
          </Col>
        </Row>
      </Grid>
      <TableUser
        removeUserId="removeUserId"
        reloadTable2={reloadTable2}
        editClick={showUpdateUserModal}
        editName="Radios"
        editClassName="radioImg"
      />
    </React.Fragment>
  );
}
