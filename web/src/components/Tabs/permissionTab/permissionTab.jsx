import React, { useState } from "react";
import {
  Panel,
  Form,
  Modal,
  TagInput,
  Whisper,
  Tooltip,
  PanelGroup,
  Row,
  Col,
  Grid,
} from "rsuite";
import Axios from "axios";
import { faUserTie, faEdit } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PrimaryBtn from "../../buttons/primaryBtn";
import SecondaryBtn from "../../buttons/secondaryBtn";
import "./permissionTab.css";
import ModalPermissionUpdate from "../../modal/permission/modalPermissionUpdate";
import Swal from "sweetalert2";

export default function PermissionTab() {
  const [newCategory, setnewCategory] = useState("");
  const [newPermission, setnewPermission] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);

  const HOST = `${process.env.REACT_APP_HOST}`;
  const SERVER_PORT = `${process.env.REACT_APP_SERVER_PORT}`;
  const PROTOCOL = `${process.env.REACT_APP_PROTOCOL}`;

  //insert new permission btn start
  const insertPermission = () => {
    if (newCategory === "" || newPermission.length === 0) {
      Swal.fire({
        position: "top-end",
        icon: "warning",
        title: "All Field Requried!",
        showConfirmButton: false,
        timer: 1500,
      });
    } else {
      Axios.post(`${PROTOCOL}://${HOST}:${SERVER_PORT}/insertPermission`, {
        Category: newCategory,
        Permission: newPermission,
      }).then(() => {
      
        Swal.fire({
          position: "top-end",
          icon: "success",
          title: "Your work has been saved",
          showConfirmButton: false,
          timer: 1500,
        });
         setnewCategory("");
         setnewPermission([]);
      });
    }
  };
  //insert new permission btn end

  //open permisssion edit modal start
  const permissionEdit = () => setModalOpen(true);
  //open permisssion edit modal end

  //close permisssion edit modal start
  const modalClose = () => setModalOpen(false);
  //close permisssion edit modal end

  //resetBtn start
  const resetBtn = () => {
    setnewCategory("");
    setnewPermission([]);
  };
  //resetBtn end

  return (
    <React.Fragment>
      <Modal open={modalOpen} onClose={modalClose} size="sm">
        <ModalPermissionUpdate />
      </Modal>

      <PanelGroup accordion shaded="true" className="panelGroup">
        <Panel
          defaultExpanded
          collapsible
          eventKey={1}
          id="panel1"
          header={
            <Grid fluid={true}>
              <Row className="show-grid">
                <Col xs={11}>
                  <span style={{ display: "inline" }}>
                    <FontAwesomeIcon icon={faUserTie} className="icon_icon" />
                    <label className="icon_label">New Permission</label>
                  </span>
                </Col>

                <Whisper
                  placement="left"
                  controlId="control-id-hover"
                  trigger="hover"
                  speaker={<Tooltip>Change permission</Tooltip>}
                >
                  <Col xs={1} xsPush={12}>
                    <FontAwesomeIcon icon={faEdit} onClick={permissionEdit} />
                  </Col>
                </Whisper>
              </Row>
            </Grid>
          }
        >
          <Grid fluid={true} className="permissionPanelGroup">
            <Row className="permissionPanel">
              <Form fluid={true}>
                <Form.Group controlId="name-1">
                  <Form.ControlLabel>Category</Form.ControlLabel>
                  <Form.Control
                    name="name"
                    placeholder="Add new category"
                    onChange={(event) => {
                      setnewCategory(event);
                    }}
                    value={newCategory}
                  />
                </Form.Group>
                <Form.Group controlId="email-1">
                  <Form.ControlLabel>Permission(s)</Form.ControlLabel>
                  <TagInput
                    block
                    value={newPermission}
                    trigger={["Enter", "Space", "Comma"]}
                    placeholder="Add new permission(s)"
                    onChange={(event) => {
                      setnewPermission(event);
                    }}
                  />
                </Form.Group>
              </Form>
            </Row>

            <Row className="show-grid" style={{ display: "inline-flex" }}>
              <PrimaryBtn value="Submit" onClick={insertPermission} />
              <SecondaryBtn value="Reset" onClick={resetBtn} />
            </Row>
          </Grid>
        </Panel>
      </PanelGroup>
    </React.Fragment>
  );
}
